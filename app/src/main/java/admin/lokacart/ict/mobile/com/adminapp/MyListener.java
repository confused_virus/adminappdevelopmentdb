package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by Vishesh on 08-01-2016.
 */
public interface MyListener {
    public void onCardClickListener(int position,int cat, Object obj);
    public void onCardLongClickListener(int position, int category, Object obj);
}
