package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by madhav on 4/6/16.
 */
public class TermsAndConditionsFragment extends Fragment {

    View termsAndConditionsFragmentView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        termsAndConditionsFragmentView = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
        getActivity().setTitle(R.string.title_fragment_terms_and_conditions);
        setHasOptionsMenu(true);

        if(TermsAndConditionsFragment.this.isAdded() && getActivity()!=null)
        ((DashboardActivity)getActivity()).updateStatusBarColor();

        return termsAndConditionsFragmentView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}
