package admin.lokacart.ict.mobile.com.adminapp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Vishesh on 08-02-2016.
 */
public class    Member
{
    int userID;
    String name, lastname, address, email, phone, pincode, role;
    boolean isA, isP, isM;

    public Member(JSONObject object)
    {
        assign(object);
        try {
            userID = object.getInt("userID");
        } catch (JSONException e) {
        }
        try {
            role = object.getString("role");
        } catch (JSONException e) {
        }
        isA = role.toLowerCase().contains("admin");
        isP = role.toLowerCase().contains("publisher");
        isM = role.toLowerCase().contains("member");
    }

    public Member(JSONObject jsonObject, int i)
    {
        //while adding a new member
        assign(jsonObject);
        try
        {
            if(jsonObject.getString("isAdmin").equals("0"))
                isA = false;
            else
                isA = true;

            if(jsonObject.getString("isPublisher").equals("0"))
                isP = false;
            else
                isP = true;

            isM = true;
        }
        catch (Exception e)
        {
        }

    }

    private void assign(JSONObject object)
    {
        try {
            name = object.getString("name");
        } catch (JSONException e) {
        }
        try {
            address = object.getString("address");
        } catch (JSONException e) {
        }
        try {
            email = object.getString("email");
        } catch (JSONException e) {
        }
        try {
            phone = object.getString("phone").substring(2);
        } catch (JSONException e) {
        }

        try {
            pincode = object.getString("pincode");
        }
        catch (Exception e) {
            pincode = null;
        }
        try{
            role = object.getString("role");
        }
        catch (Exception e){
            role = null;
        }
        try {
            lastname = object.getString("lastname");
        }
        catch (Exception e) {
            lastname = null;
        }
    }

    public void setName(String name)
    {
        this.name = name;
    }
    public void setRole(String role){
        this.role = role;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }
    public void setUserID(int userID)
    {
        this.userID = userID;
    }
    public void setPincode(String pincode)
    {
        this.pincode = pincode;
    }
    public void setA(boolean isA)
    {
        this.isA = isA;
    }
    public void setP(boolean isP)
    {
        this.isA = isP;
    }
    public void setLastname(String lastName)
    {
        this.lastname = lastName;
    }

    public String getName()
    {
        return name;
    }
    public String getRole() {
        return role;
    }
    public String getAddress()
    {
        return address;
    }
    public String getEmail()
    {
        return email;
    }
    public String getPhone()
    {
        return phone;
    }
    public String getUserID()
    {
        return userID + "";
    }
    public String getLastName() { return lastname; }
    public int getIntUserID()
    {
        return userID;
    }
    public String getPincode()
    {
        return pincode;
    }
    public boolean isA()
    {
        return isA;
    }
    public boolean isP()
    {
        return isP;
    }
    public boolean isM()
    {
        return isM;
    }
}
